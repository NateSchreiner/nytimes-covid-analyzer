import csv
import sys
from collections import OrderedDict

STATE = 'state'
STATES = 'states'
COUNTY = 'county'
COUNTIES = 'counties'


# {State : {Date : [cases, deaths]}, {Date : [cases, deaths]}}
def newStateData():
    unsorted = {}
    rowCount = 0
    with open('covid-19-data/us-states.csv', 'r', newline='') as cfile:
        reader = csv.reader(cfile, delimiter=',', quotechar='|')
        for row in reader:
            if rowCount == 0:
                rowCount = 1
                continue
            else:
                try:
                    unsorted[row[1]].update({row[0]:[int(row[3]),int(row[4])]})
                except KeyError as e:
                    unsorted[row[1]] = {row[0]:[int(row[3]), int(row[4])]}
    return unsorted


def newSortStates(unsorted):
    latest_date = findLatestDate()
    res = OrderedDict(sorted(unsorted.items(), key = lambda x: x[1][latest_date], reverse = True))
    count = 0
    for key, value in res.items():
        print(key + ' ----  CASES: ' + str(value[latest_date][0]) + ' DEATHS: ' + str(value[latest_date][1]))


def getTimeSeriesForState(state):
    unsorted = newStateData()
    dictionary = unsorted[state]
    prev = 0
    death_prev = 0
    for key,value in dictionary.items():
        change = value[0] - prev
        death_change = value[1] - death_prev
        print(key + " cases/deaths: " + str(value) + ' +/- CC: ' + str(change) + ' +/- DC: ' + str(death_change))
        prev = value[0]
        death_prev = value[1]


# Returns dictionary.. K: 'County,State', V: Case's
def getCountyData():
    unsorted = {}
    rowCount = 0
    with open('covid-19-data/us-counties.csv', 'r', newline='') as cfile:
        reader = csv.reader(cfile, delimiter=',', quotechar='|')
        for row in reader:
            if rowCount == 0:
                rowCount = 1
                continue
            else:
                # add to unsorted
                unsorted[row[1] + ',' + row[2]] = int(row[4])
    return unsorted


def getDataByDate(date, data):
    index = 0
    for row in data:
        if row[0] == date:
            break
        else:
            index = index + 1
    return data[index:]


def sortCounties():
    unsorted = getCountyData()
    sorted_list = {}
    for k in sorted(unsorted, key=unsorted.get, reverse = True):
        sorted_list[k] = unsorted[k]
    return sorted_list


def sortStateByCounty(county):
    return_dict = {}
    new_dict = {}
    sorted_dict = sortCounties()
    for k, v in sorted_dict.items():
        if k.startswith(county):
            new_dict[k] = v
    for k in sorted(new_dict, key=new_dict.get, reverse = True):
        return_dict[k] = new_dict[k]
    return return_dict


def total(stateData):
    latest_date = findLatestDate()
    total_cases = 0
    for key, value in stateData.items():
        total_cases = total_cases + value[latest_date][0]
    return total_cases


def getRowData():
    array = []
    reader = None
    rowCount = 0
    with open('covid-19-data/us-counties.csv', 'r', newline='') as cfile:
        reader = csv.reader(cfile, delimiter=',', quotechar='|')
        count = 0
        for row in reader:
            if rowCount == 0:
                rowCount = 1
                continue
            array.append(row)
    return array


def findLatestDate():
    latest = 0
    rowCount = 0
    rows = getRowData()
    for row in rows:
        if rowCount == 0:
            rowCount = 1
            continue
        dateParts = row[0].split('-')
        intDate = int(dateParts[0] + dateParts[1] + dateParts[2])
        if intDate > latest:
            latest = intDate
    date = ''
    for char in str(latest):
        date += char
        if len(date) == 4 or len(date) == 7:
            date += '-'
    return date


def prettyPrint(dic):
    for k, v in dic.items():
        print(k + ' : ' + str(v))


args = sys.argv
if args[1] == 'total':
    print("Total US Cases: " + str(total(newStateData())))

if len(args) == 2 and args[1] == STATES:
    newSortStates(newStateData())
elif len(args) == 3:
    if args[1] == COUNTY:
        prettyPrint(sortStateByCounty(args[2]))
    elif args[1] == STATE:
        getTimeSeriesForState(args[2])
else:
    if args[1] == STATE and args[3] == COUNTY:
        pass # Get Time series for county in state



