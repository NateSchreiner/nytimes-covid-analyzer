cd to any directory you want to store this repo:
    cd /home/user/you

make a new Directory to house data and script:
    mkdir script

cd into new directory:
    cd script

Clone data repo:
    git clone https://github.com/nytimes/covid-19-data.git

move up one directory:
    cd ..

copy script to current working directory:
    cp /path/to/script/aggregator.py aggregator.py




USAGE: 
    base: 
        python3 aggregator.py <SWITCH>

allowed SWITCHES:

    counties <COUNTY> --> Total Cases and Deaths as of most recent day

    states --> List sorted by most cases

    state <STATE> --> Time Series

    state <STATE> county <COUNTY> --> Time Series

    total --> Gets total cases across all states

    total time --> Time series of USA cases and deaths
    
    county <COUNTY> state <STATE> --> Specified county time series


